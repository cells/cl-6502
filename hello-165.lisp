;;;; -*- Mode: lisp; indent-tabs-mode: nil -*-
;;;
;;; tutor1.l65 --- A simple 6502 LAP assembly source file.
;;;
;;; Copyright (C) 2005, James Bielman  <jamesjb@jamesjb.com>
;;;
;;; Permission is hereby granted, free of charge, to any person
;;; obtaining a copy of this software and associated documentation
;;; files (the "Software"), to deal in the Software without
;;; restriction, including without limitation the rights to use, copy,
;;; modify, merge, publish, distribute, sublicense, and/or sell copies
;;; of the Software, and to permit persons to whom the Software is
;;; furnished to do so, subject to the following conditions:
;;;
;;; The above copyright notice and this permission notice shall be
;;; included in all copies or substantial portions of the Software.
;;;
;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;;; NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
;;; HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
;;; WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
;;; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;; DEALINGS IN THE SOFTWARE.
;;;

(defmacro defconstant-asm (name value &optional doc)
  "Define symbol NAME to have the constant value VALUE."
  `(eval-when (:assemble)
    (defconstant ,name ,value ,@(if doc (list doc)))))

(defconstant-asm +prg-load-address+ #x0801
  "The load address to put in the PRG header.")

(defconstant-asm +sys-bytecode+ #x9e
  "CBM BASIC bytecode for the SYS statement.")

(defmacro prg-header (start-label)
  "Output the standard Commodore PRG header."
  `(tagbody
      (word +prg-load-address+)
      (org  +prg-load-address+)
      (word @next 10)
      (byte +sys-bytecode+ (format nil " ~D" ,start-label) 0)
    next
      (word 0)
      (advance ,start-label)))

(defmacro asciiz (&rest things)
  "Same as BYTE but adds a null terminator at the end."
  `(byte ,@things 0))

  (prg-header 2064)
  (ldx :imm 0)
loop
  (lda @hello x)
  (beq @done)
  (jsr #xffd2)
  (inx)
  (bne @loop)
done
  (rts)

hello
  (asciiz "HELLO, WORLD!")

;;; vim: ft=lisp ts=3 et:
