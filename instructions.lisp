;;;; -*- Mode: lisp; indent-tabs-mode: nil -*-
;;;
;;; instructions.lisp --- The 6502 instruction set.
;;;
;;; Copyright (C) 2005, James Bielman  <jamesjb@jamesjb.com>
;;;
;;; Permission is hereby granted, free of charge, to any person
;;; obtaining a copy of this software and associated documentation
;;; files (the "Software"), to deal in the Software without
;;; restriction, including without limitation the rights to use, copy,
;;; modify, merge, publish, distribute, sublicense, and/or sell copies
;;; of the Software, and to permit persons to whom the Software is
;;; furnished to do so, subject to the following conditions:
;;;
;;; The above copyright notice and this permission notice shall be
;;; included in all copies or substantial portions of the Software.
;;;
;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;;; NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
;;; HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
;;; WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
;;; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;; DEALINGS IN THE SOFTWARE.
;;;

(in-package #:cl-6502)

;;;# Addressing Modes

(defvar *addressing-modes* nil
  "List of all addressing modes supported by the assembler.")

(defmacro define-addressing-mode (name &body body)
  "Define an addressing mode."
  (destructuring-bind (&key template size) body
    `(progn
      (pushnew ',name *addressing-modes*)
      (setf (get ',name 'template) ',template)
      (setf (get ',name 'size) ,size))))

;;;## Implied

(define-addressing-mode :implied
  :size 1
  :template ())

;;;## Accumulator

(define-addressing-mode :accumulator
  :size 1
  :template ((:symbol a)))

;;;## Immediate

(define-addressing-mode :immediate
  :size 2
  :template ((:keyword :imm) (:operand 8)))

;;;## Zero Page

(define-addressing-mode :zero-page
  :size 2
  :template ((:operand 8)))

;;;## Zero Page, X

(define-addressing-mode :zero-page-x
  :size 2
  :template ((:operand 8) (:symbol x)))

;;;## Zero Page, Y

(define-addressing-mode :zero-page-y
  :size 2
  :template ((:operand 8) (:symbol y)))

;;;## Relative

(define-addressing-mode :relative
  :size 2
  :template ((:operand 16)))

;;;## Absolute

(define-addressing-mode :absolute
  :size 3
  :template ((:operand 16)))

;;;## Absolute, X

(define-addressing-mode :absolute-x
  :size 3
  :template ((:operand 16) (:symbol x)))

;;;## Absolute, Y

(define-addressing-mode :absolute-y
  :size 3
  :template ((:operand 16) (:symbol y)))

;;;## Indirect

(define-addressing-mode :indirect
  :size 3
  :template ((:keyword :indirect) (:operand 16)))

;;;## Indirect, X

(define-addressing-mode :indirect-x
  :size 2
  :template ((:keyword :indirect) (:operand 8) (symbol x)))

;;;## Indirect, Y

(define-addressing-mode :indirect-y
  :size 2
  :template ((:keyword :indirect) (:operand 8) (symbol y)))

;;;# LAP Instruction Decoding

(defun symbol-equalp (sym1 sym2)
  "Return true if SYM1 and SYM2 have the same name."
  (equalp (symbol-name sym1) (symbol-name sym2)))

(defun template-expr-matches-p (iexpr texpr)
  "Return true if LAP expression IEXPR matches a template expression."
  (destructuring-bind (type arg) texpr
    (ecase type
      (:keyword
       (eq iexpr arg))
      (:symbol
       (and (symbolp iexpr) (symbol-equalp iexpr arg)))
      (:operand
       (let ((value (eval iexpr)))
         (values (typep value `(unsigned-byte ,arg)) value))))))

(defmethod template-matches-p ((a assembler) template args)
  "Return two values; true and the operand if ARGS match TEMPLATE."
  (unless (= (length template) (length args))
    (return-from template-matches-p nil))
  (iter (with operand = nil)
        (for texpr :in template)
        (for iexpr :in args)
        (multiple-value-bind (winp value)
            (template-expr-matches-p iexpr texpr)
          (unless winp
            (return nil))
          (when value
            (setf operand value)))
        (finally (return (values t operand)))))

(defmethod addressing-mode ((a assembler) form)
  "Return two values; the addressing mode of LAP expression FORM, the
value of the operand."
  (destructuring-bind (op &rest args) form
    (iter (for mode :in (insn-addressing-modes op))
          (multiple-value-bind (winp operand)
              (template-matches-p a (get mode 'template) args)
            (when winp
              (return-from addressing-mode (values mode operand)))))))

(defmethod decode-lap-insn ((a assembler) form)
  "Parse a LAP form and return four values; the addressing mode, the
instruction opcode, the operand as an integer, if any, and the
instruction size.  If multiple addressing modes are possible, the
first mode found will be returned."
  (multiple-value-bind (mode operand) (addressing-mode a form)
    (values mode (insn-opcode (car form) mode) operand (get mode 'size))))

;;;# Instructions

(defmacro define-instruction (name &body address-modes)
  "Define the properties of a 6502 instruction for the assembler."
  (let (doc)
    (when (stringp (car address-modes))
      (setf doc (pop address-modes)))
    `(progn
      (setf (get ',name 'instruction) ',address-modes)
      (setf (get ',name 'instruction-documentation) ,doc))))

(defun insn-addressing-modes (mnemonic)
  "Return a list of possible addressing modes for MNEMONIC."
  (mapcar #'car (get mnemonic 'instruction)))

(defun insn-opcode (mnemonic addressing-mode)
  "Return the opcode for MNEMONIC in ADDRESSING-MODE."
  (iter (for (name opcode) :in (get mnemonic 'instruction))
        (finding opcode :such-that (eq addressing-mode name))))

;;;## ADC

(define-instruction adc
  "Add with Carry"
  (:immediate   #x69)
  (:zero-page   #x65)
  (:zero-page-x #x75)
  (:absolute    #x6d)
  (:absolute-x  #x7d)
  (:absolute-y  #x79)
  (:indirect-x  #x61)
  (:indirect-y  #x71))

;;;## AND

(define-instruction and
  "Logical AND"
  (:immediate   #x29)
  (:zero-page   #x25)
  (:zero-page-x #x35)
  (:absolute    #x2d)
  (:absolute-x  #x3d)
  (:absolute-y  #x39)
  (:indirect-x  #x21)
  (:indirect-y  #x31))

;;;## ASL

(define-instruction asl
  "Arithmetic Shift Left"
  (:accumulator #x0a)
  (:zero-page   #x06)
  (:zero-page-x #x16)
  (:absolute    #x0e)
  (:absolute-x  #x1e))

;;;## BCC

(define-instruction bcc
  "Branch if Carry Clear"
  (:relative    #x90))

;;;## BCS

(define-instruction bcs
  "Branch if Carry Set"
  (:relative    #xb0))

;;;## BEQ

(define-instruction beq
  "Branch if Equal"
  (:relative    #xf0))

;;;## BIT

(define-instruction bit
  "Bit Test"
  (:zero-page   #x24)
  (:absolute    #x2c))

;;;## BMI

(define-instruction bmi
  "Branch if Minus"
  (:relative    #x30))

;;;## BNE

(define-instruction bne
  "Branch if Not Equal"
  (:relative    #xd0))

;;;## BPL

(define-instruction bpl
  "Branch if Positive"
  (:relative    #x10))

;;;## BRK

(define-instruction brk
  "Force Interrupt"
  (:implied     #x00))

;;;## BVC

(define-instruction bvc
  "Branch if Overflow Clear"
  (:relative    #x50))

;;;## BVS

(define-instruction bvs
  "Branch if Overflow Set"
  (:relative    #x70))

;;;## CLC

(define-instruction clc
  "Clear Carry Flag"
  (:implied     #x18))

;;;## CLD

(define-instruction cld
  "Clear Decimal Mode"
  (:implied     #xd8))

;;;## CLI

(define-instruction cli
  "Clear Interrupt Disable"
  (:implied     #x58))

;;;## CLV

(define-instruction clv
  "Clear Overflow Flag"
  (:implied     #xb8))

;;;## CMP

(define-instruction cmp
  "Compare"
  (:immediate   #xc9)
  (:zero-page   #xc5)
  (:zero-page-x #xd5)
  (:absolute    #xcd)
  (:absolute-x  #xdd)
  (:absolute-y  #xd9)
  (:indirect-x  #xc1)
  (:indirect-y  #xd1))

;;;## CPX

(define-instruction cpx
  "Compare X Register"
  (:immediate   #xe0)
  (:zero-page   #xe4)
  (:absolute    #xec))

;;;## CPY

(define-instruction cpy
  "Compare Y Register"
  (:immediate   #xc0)
  (:zero-page   #xc4)
  (:absolute    #xcc))

;;;## DEC

(define-instruction dec
  "Decrement Memory"
  (:zero-page   #xc6)
  (:zero-page-x #xd6)
  (:absolute    #xce)
  (:absolute-x  #xde))

;;;## DEX

(define-instruction dex
  "Decrement X Register"
  (:implied     #xca))

;;;## DEY

(define-instruction dey
  "Decrement Y Register"
  (:implied     #x88))

;;;## EOR

(define-instruction eor
  "Exclusive OR"
  (:immediate   #x49)
  (:zero-page-x #x55))

;;;## INC

(define-instruction inc
  "Increment Memory"
  (:zero-page   #xe6)
  (:zero-page-x #xf6)
  (:absolute    #xee)
  (:absolute-x  #xfe))

;;;## INX

(define-instruction inx
  "Increment X Register"
  (:implied     #xe8))

;;;## INY

(define-instruction iny
  "Increment Y Register"
  (:implied     #xc8))

;;;## JMP

(define-instruction jmp
  "Jump"
  (:absolute    #x4c)
  (:indirect    #x6c))

;;;## JSR

(define-instruction jsr
  "Jump to Subroutine"
  (:absolute    #x20))

;;;## LDA

(define-instruction lda
  "Load Accumulator"
  (:immediate   #xa9)
  (:zero-page   #xa5)
  (:zero-page-x #xb5)
  (:absolute    #xad)
  (:absolute-x  #xbd)
  (:absolute-y  #xb9)
  (:indirect-x  #xa1)
  (:indirect-y  #xb1))

;;;## LDX

(define-instruction ldx
  "Load X Register"
  (:immediate   #xa2)
  (:zero-page   #xa6)
  (:zero-page-y #xb6)
  (:absolute    #xae)
  (:absolute-y  #xbe))

;;;## LDY

(define-instruction ldy
  "Load Y Register"
  (:immediate   #xa0)
  (:zero-page   #xa4)
  (:zero-page-x #xb4)
  (:absolute    #xac)
  (:absolute-x  #xbc))

;;;## LSR

(define-instruction lsr
  "Logical Shift Right"
  (:accumulator #x4a)
  (:zero-page   #x46)
  (:zero-page-x #x56)
  (:absolute    #x4e)
  (:absolute-x  #x5e))

;;;## NOP

(define-instruction nop
  "No Operation"
  (:implied     #xea))

;;;## ORA

(define-instruction ora
  "Logical Inclusive OR"
  (:immediate   #x90)
  (:zero-page   #x05)
  (:zero-page-x #x15)
  (:absolute    #x0d)
  (:absolute-x  #x1d)
  (:absolute-y  #x19)
  (:indirect-x  #x01)
  (:indirect-y  #x11))

;;;## PHA

(define-instruction pha
  "Push Accumulator"
  (:implied     #x48))

;;;## PHP

(define-instruction php
  "Push Processor Status"
  (:implied     #x08))

;;;## PLA

(define-instruction pla
  "Pull Accumulator"
  (:implied     #x68))

;;;## PLP

(define-instruction plp
  "Pull Processor Status"
  (:implied     #x28))

;;;## ROL

(define-instruction rol
  "Rotate Left"
  (:accumulator #x2a)
  (:zero-page   #x26)
  (:zero-page-x #x36)
  (:absolute    #x2e)
  (:absolute-x  #x3e))

;;;## ROR

(define-instruction ror
  "Rotate Right"
  (:accumulator #x6a)
  (:zero-page   #x66)
  (:zero-page-x #x76)
  (:absolute    #x6e)
  (:absolute-x  #x7e))

;;;## RTI

(define-instruction rti
  "Return from Interrupt"
  (:implied     #x40))

;;;## RTS

(define-instruction rts
  "Return from Subroutine"
  (:implied     #x60))

;;;## SBC

(define-instruction sbc
  "Subtract with Carry"
  (:immediate   #xe9)
  (:zero-page   #xe5)
  (:zero-page-x #xf5)
  (:absolute    #xed)
  (:absolute-x  #xfd)
  (:absolute-y  #xf9)
  (:indirect-x  #xe1)
  (:indirect-y  #xf1))

;;;## SEC

(define-instruction sec
  "Set Carry Flag"
  (:implied     #x38))

;;;## SED

(define-instruction sed
  "Set Decimal Flag"
  (:implied     #xf8))

;;;## SEI

(define-instruction sei
  "Set Interrupt Disable"
  (:implied     #x78))

;;;## STA

(define-instruction sta
  "Store Accumulator"
  (:zero-page   #x85)
  (:zero-page-x #x95)
  (:absolute    #x8d)
  (:absolute-x  #x9d)
  (:absolute-y  #x99)
  (:indirect-x  #x81)
  (:indirect-y  #x91))

;;;## STX

(define-instruction stx
  "Store X Register"
  (:zero-page   #x86)
  (:zero-page-y #x96)
  (:absolute    #x8e))

;;;## STY

(define-instruction sty
  "Store Y Register"
  (:zero-page   #x84)
  (:zero-page-x #x94)
  (:absolute    #x8c))

;;;## TAX

(define-instruction tax
  "Transfer Accumulator to X"
  (:implied     #xaa))

;;;## TAY

(define-instruction tay
  "Transfer Accumulator to Y"
  (:implied     #xa8))

;;;## TSX

(define-instruction tsx
  "Transfer Stack Pointer to X"
  (:implied     #xba))

;;;## TXA

(define-instruction txa
  "Transfer X to Accumulator"
  (:implied     #x8a))

;;;## TXS

(define-instruction txs
  "Transfer X to Stack Pointer"
  (:implied     #x9a))

;;;## TYA

(define-instruction tya
  "Transfer Y to Accumulator"
  (:implied     #x98))
