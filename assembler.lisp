;;;; -*- Mode: lisp; indent-tabs-mode: nil -*-
;;;
;;; assembler.lisp --- A 6502 assembler using Cells.
;;;
;;; Copyright (C) 2005, James Bielman  <jamesjb@jamesjb.com>
;;;
;;; Permission is hereby granted, free of charge, to any person
;;; obtaining a copy of this software and associated documentation
;;; files (the "Software"), to deal in the Software without
;;; restriction, including without limitation the rights to use, copy,
;;; modify, merge, publish, distribute, sublicense, and/or sell copies
;;; of the Software, and to permit persons to whom the Software is
;;; furnished to do so, subject to the following conditions:
;;;
;;; The above copyright notice and this permission notice shall be
;;; included in all copies or substantial portions of the Software.
;;;
;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;;; NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
;;; HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
;;; WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
;;; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;; DEALINGS IN THE SOFTWARE.
;;;

(in-package #:cl-6502)



(defun test-6502-assemble ()
  (cell-reset)
  (assemble-file "/0dev/cl-6502/hello.l65"))


;;;# Utilities
;;;
;;;## Integer Types
;;;
;;; The Lisp types defined below are shorthands for the standard 8-bit
;;; and 16-bit integer types, both signed and unsigned.  The `I8' and
;;; `I16' types represent either a signed or unsigned integer.

(deftype u8 () `(unsigned-byte 8))
(deftype s8 () `(signed-byte 8))
(deftype i8 () `(or u8 s8))
(deftype u16 () `(unsigned-byte 16))
(deftype s16 () `(signed-byte 16))
(deftype i16 () `(or u16 s16))

;;;## Integer Conversions
;;;
;;; These functions convert numbers to unsigned 8- or 16-bit integers
;;; for placement in object code.  Signed integers are converted to
;;; their 2s complement representation.

(defun u8 (i8)
  "Convert an 8-bit integer to an unsigned 8-bit integer."
  (logand i8 #xff))

(defun u16 (i16)
  "Convert a 16-bit integer to an unsigned 16-bit integer."
  (logand i16 #xffff))

;;;## Byte Vectors
;;;
;;; The following functions are shorthand for creating vectors of
;;; unsigned bytes and concatenating those vectors.  Use
;;; `MAKE-BYTE-BUFFER' to grow a buffer when the length is not known,
;;; or `MAKE-BYTE-VECTOR' when the length is known ahead of time.

(defun make-byte-vector (length)
  "Create a byte vector of LENGTH suitable for object code."
  (make-array length :element-type 'u8 :initial-element 0))

(defun make-byte-buffer (&optional (length 0))
  "Create an adjustable byte buffer of LENGTH with a fill pointer."
  (make-array length :element-type 'u8 :adjustable t :fill-pointer 0))

(defun vconc (v1 v2)
  "Concatenate two vectors and return the resulting vector."
  (concatenate 'vector v1 v2))

;;;# 6502 Assembler
;;;
;;; An `ASSEMBLER' instance contains the state of a 6502 assembler.
;;; It is possible to run multiple assemblers in parallel by creating
;;; multiple instances of this class.  However, they will share the
;;; same instruction set and assembler directives.
;;;
;;; As instructions and directives are assembled they are converted
;;; into an `IR-NODE' or its subclass `IR-INSN'.  These classes are
;;; Cells models---they automatically recompute their state based on
;;; the preceding instructions and the assembler's symbol table.

(defvar *assembler* nil
  "The current executing assembler, bound in ASSEMBLE.")

(defclass assembler ()
  ((nodes
    :initform (make-array 0 :adjustable t :fill-pointer 0)
    :accessor nodes
    :documentation "Array of IR-NODE instances to assemble.")
   (org-used-p
    :initform nil
    :accessor org-used-p
    :documentation "Set to true once an ORG directive is used.")
   (globals
    :initform (make-hash-table)
    :reader globals
    :documentation "Hash table of globally defined labels.")
   (macros
    :initform (make-hash-table)
    :reader macros
    :documentation "Macro functions defined in the assembler."))
  (:documentation "A 6502 assembler."))

(defmethod initialize-instance :after ((a assembler) &key)
  "Perform post-initialization tasks after creating an assembler."
  (bootstrap-macros a))

(defmethod pc ((a assembler))
  "Return the program counter of assembler A."
  (let* ((vec (nodes a))
         (last (fill-pointer vec)))
    (if (plusp last)
        (let ((node (aref vec (1- last))))
          (+ (relative-location node) (size-in-bytes node)))
        0)))

(defmethod object-code ((assembler assembler))
  "Return the total object code for all nodes in ASSEMBLER."
  (reduce #'vconc (map 'list #'object-code (nodes assembler))))

(defmethod reset ((a assembler))
  "Reset ASSEMBLER to a pristine set, removing all nodes."
  (setf (nodes a) (make-array 0 :adjustable 0 :fill-pointer 0))
  (clrhash (globals a))
  (clrhash (macros a))
  (bootstrap-macros a))

;;;## Reader Macros
;;;
;;; The assembler sets up a custom readtable with a macro character
;;; `@', which is placed before a symbol to denote a label reference.
;;; The `@' is not used when defining a label.

(defun at-reader (stream char)
  "Reader macro for the @ macro character."
  (declare (ignore char))
  (let ((sym (read stream t nil t)))
    (etypecase sym
      (symbol `(cl-6502::global-label-location *assembler* ',sym)))))

(defun make-assembler-readtable ()
  "Create the assembler readtable."
  (let ((rt (copy-readtable nil)))
    (set-macro-character #\@ 'at-reader t rt)
    rt))

(defparameter *assembler-readtable* (make-assembler-readtable)
  "Readtable for LAP assembly syntax.")

;;;## File Assembler
;;;
;;; High-level interface for assembling complete files.

(defun assemble-file-pathname (pathname)
  "Return the pathname of the compiled version of PATHNAME."
  (make-pathname :type "bin" :defaults pathname))

(defun assemble-file (file &key (output-file (assemble-file-pathname file)))
  "Assemble FILE and write the object code to OUTPUT-FILE."
  (format *trace-output* ";; Assembling file ~S:~%" file)
  (let ((*assembler* (make-instance 'assembler))
        (*readtable* *assembler-readtable*)
        (*package* #.*package*))
    (with-open-file (in file :direction :input)
      (iter (for form :in-stream in)
            (assemble *assembler* form)))
    (with-open-file (out output-file :direction :output
                         :if-exists :supersede :element-type 'u8)
      (let ((code (object-code *assembler*)))
        (format *trace-output* ";; Writing ~D byte~:P to ~S.~%"
                (length code) output-file)
        (write-sequence (object-code *assembler*) out)))
    output-file))

;;;## Macros
;;;
;;; Low-level functions for defining and expanding assembler macros.

(defmethod assembler-macro-function ((a assembler) name)
  "Return the assembler macro function in A for NAME."
  (gethash name (macros a)))

(defmethod (setf assembler-macro-function) (value (a assembler) name)
  "Set the assembler macro function for NAME in A to VALUE."
  (setf (gethash name (macros a)) value))

(defun expand-defmacro (args)
  "Expander for the DEFMACRO macro, used to bootstrap macros."
  (destructuring-bind (name lambda-list &rest body) args
    (let ((whole (gensym "WHOLE"))
          (decls nil) (doc nil))
      (if (stringp (car body))
          (setf doc (pop body)))
      (iter (while (eq (caar body) 'declare))
            (push (pop body) decls))
      `(eval-when (:assemble)
        (setf (assembler-macro-function *assembler* ',name)
         #'(lambda (,whole)
             ,@(if doc (list doc))
             ,@decls
             (destructuring-bind ,lambda-list ,whole
               ,@body)))))))

(defmethod bootstrap-macros ((a assembler))
  "Bootstrap the macro system by defining the DEFMACRO expander."
  (setf (assembler-macro-function a 'defmacro) #'expand-defmacro))

(defmethod assembler-macroexpand ((a assembler) form)
  "Repeatedly expand macros in FORM until none are left."
  (iter (for op = (car form))
        (for func = (assembler-macro-function a op))
        (while func)
        (setf form (funcall func (rest form)))
        (finally (return form))))

;;;## Internals
;;;
;;; Low-level functions for assembling individual forms.  These are
;;; called by `ASSEMBLE-FILE' for each form read from the input file.

(defmethod append-node ((assembler assembler) node-class &rest initargs)
  "Create a node of NODE-CLASS passing INITARGS and push it onto the
end of ASSEMBLER's node vector.  This automatically adds the :INDEX
and :ASSEMBLER keyword arguments to MAKE-INSTANCE."
  (let* ((vec (nodes assembler))
         (node (apply #'make-be node-class :assembler assembler
                      :index (fill-pointer vec) initargs)))
    (vector-push-extend node vec)))

(defmethod assemble ((a assembler) form)
  "Process a LAP assembler form in assembler A."
  (etypecase form
    (cons
     (setf form (assembler-macroexpand a form))
     (destructuring-bind (op &rest args) form
       (if (find-assembler-directive op)
           (assemble-directive a op args)
           (assemble-lap-insn a form))))
    (symbol
     (notice-label-definition a form))))

;;;# IR Nodes
;;;
;;; Each instruction or assembler directive is converted into an
;;; `IR-NODE' model, which has outputs for determining its relative
;;; position in the output file, its size, and how to convert it into
;;; object code.

(defmodel ir-node ()
  ((assembler
    :initform (error "Must supply an :ASSEMBLER for IR-NODE.")
    :initarg :assembler
    :reader assembler
    :documentation "Back-pointer to the assembler that owns this node.")
   (index
    :initform (c-in 0)
    :initarg :index
    :accessor index
    :cell t
    :documentation "Index in ASSEMBLER's instruction vector.")
   (size-in-bytes
    :initform (c? 0)
    :initarg :size-in-bytes
    :reader size-in-bytes
    :cell t
    :documentation "Output: Size of this node in object code.")
   (relative-location
    :initform (c? (calculate-relative-location self))
    :initarg :relative-location
    :reader relative-location
    :cell t
    :documentation "Output: Location of this node in object code.")
   (object-code
    :initform (c? (make-byte-vector 0))
    :initarg :object-code
    :reader object-code
    :cell t
    :unchanged-if #'equalp
    :documentation "Output: Vector of bytes of object code."))
  (:documentation "A basic IR node, used for assembler directives."))

(defmethod print-object ((node ir-node) stream)
  "Print a NODE instance to STREAM unreadably."
  (print-unreadable-object (node stream :type t :identity nil)
    (format stream "#~D at ~4,'0X" (index node) (relative-location node))))

(def-c-output size-in-bytes ((self ir-node) new old oldp)
  "Output called when the size of an IR-NODE changes.  When this
happens, the location of all labels defined after this node are
adjusted to account for their new position."
  (when old
    (format *trace-output* "~&;; Size of ~A changed from ~D to ~D bytes.~%"
            self old new)
    (let ((loc (relative-location self))
          (diff (- new old)))
      (format *trace-output* "~&;; Adjusting labels after ~4,'0X by ~D:~%" loc diff)
      (iter (for (name label) :in-hashtable (globals (assembler self)))
            (when (> (location label) loc)
              (format *trace-output* "~&;;  ~S~%" name)
              (incf (location label) diff))))))

(def-c-output object-code ((self ir-node) new old oldp)
  "Output called when a node's object code changes."
  (when old
    (format *trace-output* "~&;; Rewrote object code of ~A to ~S.~%"
            self new)))

(defmethod calculate-relative-location ((node ir-node))
  "Calculate NODE's relative location based on the preceding node."
  (let ((prev (previous-node node)))
    (if prev
        (+ (relative-location prev) (size-in-bytes prev))
        0)))

(defmethod previous-node ((node ir-node))
  "Return the node preceding NODE in its assembler or nil."
  (unless (zerop (index node))
    (aref (nodes (assembler node)) (1- (index node)))))

;;;## Instruction Nodes
;;;
;;; `IR-INSN' nodes are `IR-NODE's that calculate their object code by
;;; parsing a LAP expression containing a 6502 instruction.

(defmodel ir-insn (ir-node)
  ((lap-form
    :initform (c-in nil)
    :initarg :lap-form
    :accessor lap-form
    :cell t
    :unchanged-if #'equal
    :documentation "Input: LAP instruction to assemble."))
  (:documentation "IR node subclass for 6502 instructions."))

(defmethod print-object ((node ir-insn) stream)
  "Print an instruction IR node to STREAM."
  (print-unreadable-object (node stream :type t :identity nil)
    (format stream "#~D at ~4,'0X (~A ...)"
            (index node) (relative-location node)
            (car (lap-form node)))))

(defmethod insn-object-code ((node ir-insn))
  "Return the object code for a LAP instruction."
  (multiple-value-bind (mode opcode operand size)
      (decode-lap-insn (assembler node) (lap-form node))
    (unless opcode
      (error "Unrecognized instruction ~S" (lap-form node)))
    (when (eq mode :relative)
      (setf operand (- operand 2 (+ (relative-location node)))))
    (let ((code (make-byte-vector size)))
      (setf (aref code 0) opcode)
      (ecase size
        (1 nil)
        (2 (setf (aref code 1) (u8 operand)))
        (3 (setf (aref code 1) (ldb (byte 8 0) operand))
           (setf (aref code 2) (ldb (byte 8 8) operand))))
      code)))

(defmethod assemble-lap-insn ((a assembler) expr)
  "Process a LAP instruction in assembler A."
  (append-node a 'ir-insn :lap-form expr
               :size-in-bytes (c? (length (object-code self)))
               :object-code (c? (insn-object-code self))))

;;;# Labels
;;;
;;; Labels are represented by `LABEL' instances, which store the label
;;; name of the current location of the label.  Currently this uses
;;; 0 as the location for forward-referenced labels; this doesn't
;;; provide an easy way to detect undefined label references.

(defmodel label ()
  ((name
    :initarg :name
    :reader name
    :documentation "Name of this label, a symbol.")
   (location
    :initform (c-input (:cyclicp :run-on) 0)
    :initarg :location
    :reader location
    :cell t
    :documentation "Output: Location of the label, nil if unknown."))
  (:documentation "Information about a label in the assembler."))

(defmethod print-object ((label label) stream)
  "Print a LABEL instance to STREAM unreadably."
  (print-unreadable-object (label stream :type t :identity nil)
    (format stream "~S ~4,'0X" (name label) (location label))))

(def-c-output location ((self label) new old)
  (when new
    (format *trace-output* "~&;; Moving label ~A to ~4,'0X~%" (name self) new)))

(defmethod global-label-entry ((a assembler) name)
  "Return the label entry for NAME in the assemblers global table.  If
an entry does not exist, a new entry with an undefined value will be
added and returned."
  (let ((entry (gethash name (globals a))))
    (unless entry
      (setf entry (make-be 'label :name name))
      (setf (gethash name (globals a)) entry))
    entry))

(defmethod global-label-location ((a assembler) name)
  "Return the location of label NAME in assembler A."
  (location (global-label-entry a name)))

(defmethod (setf global-label-location) (loc (a assembler) name)
  "Set the index of label NAME in assembler A to LOC."
  (setf (location (global-label-entry a name)) loc))

(defmethod notice-label-definition ((a assembler) name)
  "Notice a definition for label NAME at A's current location."
  (setf (global-label-location a name) (pc a)))

;;;# Assembler Directives
;;;
;;; Assembler directives are analogous to special forms in Lisp.
;;; Functions defined using `DEFINE-ASSEMBLER-DIRECTIVE' are passed
;;; their arguments, unevaluated.
;;;
;;; Some directives add IR nodes to the assembler to perform their
;;; work, such as the `BYTE' and `WORD' directives.  Others may just
;;; evaluate their arguments (`EVAL-WHEN'), or modify the state of the
;;; assembler in some other way.

(defmacro define-assembler-directive (name lambda-list &body body)
  "Define a function that implements an assembler directive.  It
should return either an IR-NODE instance or nil if the directive does
not result in assembling anything.  *ASSEMBLER* will be bound to the
currently running assembler."
  (let (doc decls)
    (when (stringp (car body))
      (setf doc (car body))
      (pop body))
    (iter (while (eq (caar body) 'declare))
          (push (pop body) decls))
    `(progn
      (setf (get ',name 'assembler-directive-documentation) ,doc)
      (setf (get ',name 'assembler-directive)
       (lambda ,lambda-list
         ,@decls
         (block ,name
           ,@body))))))

(defun find-assembler-directive (name)
  "Return the function for an assembler directive NAME or nil."
  (get name 'assembler-directive))

(defmethod assemble-directive ((a assembler) name args)
  "Process an assembler directive (NAME ARGS) in assembler A."
  (let ((func (find-assembler-directive name)))
    (unless func
      (error "Undefined assembler directive: ~S" name))
    (let ((*assembler* a))
      (apply func args))))

;;;## EVAL-WHEN
;;;
;;; This is the most important directive defined by the assembler.  It
;;; allows the assembly language programmer to evaluate arbitrary Lisp
;;; code at assembly time.
;;;
;;; Currently, the only situation supported is `:ASSEMBLE'.  This may
;;; change in the future.

(define-assembler-directive eval-when (situations &rest body)
  "Evaluate Lisp forms in BODY during assembly."
  (when (member :assemble situations)
    (mapc #'eval body)))

;;;## PROGN
;;;
;;; This assembler directive assembles each of its arguments without
;;; creating a new lexical environment.

(define-assembler-directive progn (&rest body)
  "Assemble forms in BODY sequentially."
  (dolist (form body)
    (assemble *assembler* form)))

;;;## TAGBODY
;;;
;;; The `TAGBODY' directive assembles each of its arguments in a new
;;; lexical scope.  (Not yet implemented).

(define-assembler-directive tagbody (&rest body)
  "Assemble forms in BODY sequentially in a new lexical environment."
  (dolist (form body)
    (assemble *assembler* form)))

;;;## BYTE
;;;
;;; The `BYTE' directive outputs literal bytes in the object code.  It
;;; accepts zero or more arguments, each of which may be either an
;;; integer, a Lisp character, or a string.
;;;
;;; Each integer, character, or character in a string, is converted to
;;; an 8-bit byte (using the Lisp's presumably ASCII-compatible
;;; encoding) and added to the object code.

(defmethod object-code-for-bytes ((node ir-node) things)
  "Return the object code THINGS as bytes in NODE."
  (let ((buf (make-byte-buffer (length things))))
    (iter (for thing :in things)
          (for result = (eval thing))
          (etypecase result
            (i8
             (vector-push-extend (u8 result) buf))
            (character
             (vector-push-extend (char-code result) buf))
            (string
             (iter (for ch :in-string result)
                   (vector-push-extend (char-code ch) buf)))))
    buf))

(define-assembler-directive byte (&rest things)
  "Emit zero or more bytes directly into the object code."
  (append-node *assembler* 'ir-node
               :size-in-bytes (c? (length (object-code self)))
               :object-code (c? (object-code-for-bytes self things))))

;;;## WORD

(defmethod object-code-for-words ((node ir-node) things)
  "Return the object code THINGS as words in NODE."
  (let ((buf (make-byte-buffer (* 2 (length things)))))
    (iter (for thing :in things)
          (for result = (eval thing))
          (etypecase result
            (i16
             (vector-push-extend (ldb (byte 8 0) result) buf)
             (vector-push-extend (ldb (byte 8 8) result) buf))))
    buf))

(define-assembler-directive word (&rest things)
  "Emit zero or more 16-bit words in little-endian byte order."
  (append-node *assembler* 'ir-node
               :size-in-bytes (c? (length (object-code self)))
               :object-code (c? (object-code-for-words self things))))

;;;## ORG

(define-assembler-directive org (loc)
  "Set the origin for further instructions to LOC."
  (when (org-used-p *assembler*)
    (error "Cannot jump around with ORG, use ADVANCE."))
  (append-node *assembler* 'ir-node :relative-location (eval loc))
  (setf (org-used-p *assembler*) t))

;;;## ADVANCE

(defmethod object-code-for-advance ((node ir-node) location)
  "Generate padding bytes from NODE to LOCATION."
  (let ((distance (- location (relative-location node))))
    (if (plusp distance)
        (make-byte-vector distance)
        (make-byte-vector 0))))

(define-assembler-directive advance (loc)
  "Create an IR node that skips ahead to LOC."
  (append-node *assembler* 'ir-node
               :size-in-bytes (c? (length (object-code self)))
               :object-code (c? (object-code-for-advance self (eval loc)))))
